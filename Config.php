<?php

final class Config {

    public static function get ( $name ) {
        if (  file_exists(__DIR__ ."/config/$name.php") ) {
             return require_once __DIR__ ."/config/$name.php";
        }
    }
}
