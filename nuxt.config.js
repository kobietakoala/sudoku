export default { 
  loading: false,
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ]
  }, 
  env: {
    users: [
      { id: 0, name: 'Samouczek'},
      { id: 1, name: 'Latwy' },
      { id: 2, name: 'Trudny'},
      { id: 3, name: 'Niemożliwy'},
    ]
  },
  generate: {
    routes: [
      '/0',
      '/1',
      '/2',
      '/3',
    ]
  },
   /*
  ** Nuxt.js modules
  */
 modules: [
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma', 
  ],

}
